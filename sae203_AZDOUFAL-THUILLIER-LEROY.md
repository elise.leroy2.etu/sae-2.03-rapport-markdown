---
title: Compte-rendu intermédiaire SAé 203 - corrigé
author: Elise LEROY, Valentin THUILLIER, Anass AZDOUFAL
date: 25-03-2023
---

# Table des matières

\tableofcontents

# Questions de culture informatique

## Questions 1 : Configuration matérielle dans VirtualBox

### Que signifie “64-bit” dans “Debian 64-bit” ?

Pour comprendre la signification de "64-bit" sur un logiciel, il faut d'abord se pencher sur le fonctionnement d'un processeur.  
Dans un disque dur par exemple, les données sont rangées par adresse mémoire, en bit. Pour qu'un processeur puisse accéder cette mémoire il faut qu'il passe par un **registre**. La taille d'un registre permet de déterminer le nombre d'adresse mémoire que le processeur peut accéder et cette taille s'écrit de la forme $2^n$ . Ainsi, les registres de $2^{32}$ octets pouvait donner accès à une adresse mémoire d'une taille maximum de $2^{32}$ octets soit 4GB.  
Un processeur communique avec son registre grâce à un **set d'instructions** faisant la même taille que le registre. Ainsi lorsque les premiers processeurs à 64 octets de registre sont apparus, il eut des problèmes de compatibilités avec les précédents processeurs de 32 octets puisque le set d'instructions était incapable de les lire. Ce faisant, les modèles suivant eurent des registres de 64 octets composés de sets d'instructions de 32 octets avec des extensions permettant d'aller jusqu'à 64 octets.

Pour bénéficier de toute la capacité du processeur, les logiciels sont écris de façon à être compatible avec une architecture de 32 ou 64 bit, d'où "Debian 64-bit". Le système d'extensions dans les sets d'instruction ont aussi permit la compatibilité descendente des logiciels, c'est à dire qu'un logiciel 32 bit pourra fonctionner sur un processeur de 64 bit mais pas l'inverse [^1] [^2] .

### Quelle est la configuration réseau utilisée par défaut ?

Par défaut, la configuration réseau d'une machine virtuelle peut varier selon le type de virtualisation utilisée. En général, la machine virtuelle émule une carte réseau connectée à un réseau virtuel qui peut être bridgé sur une interface physique de l'hôte ou utilisé en mode NAT (Network Address Translation).

Par défaut sous Debian, le nom de l'interface réseau par défaut peut être différent selon les versions et les configurations, mais elle est généralement nommée "eth0" ou "enp0s3" et est configurée en mode DHCP

### Quel est le nom du fichier XML contenant la configuration de votre machine ?

Le fichier se trouve dans :  
`/usr/local/virtual_machine/infoetu/nomUtilisateur`

Le fichier XML s'appelle **sae203.vbox**.

### Sauriez-vous le modifier directement ce fichier pour mettre 2 processeurs à votre machine ? Faites-le.

Un fichier XML est un fichier de paramétrage, nous pouvons donc modifier les paramètres du processeur.  
Pour modifier le processeur de la machine virtuelle il faut taper la commande :  
`nano /usr/local/virtual_machine/infoetu/nomUtilisateur/sae203.vbox`
Puis changer la valur du processeur au nombre de coeur souhaité.

## Questions 2 : Installation OS de base

### Qu’est-ce qu’un fichier iso bootable ?

Un fichier ISO (***I****nternational* ***S****tandards* ***O****rganization*) est une sorte de fichier d'archive, une archive étant un fichier dans lequel se trouve tout le contenu d'un dossier (fichiers, arborescence et droits d'accès) [^3] , reproduisant un disque (en général CD, DVD) sous la forme d'un fichier, on appelle cela une **image**.  
Aujourd'hui ces fichiers sont principalement utilisés dans la distribution de systèmes d'exploitation, en recopiant le contenu du média d'installation. Ainsi, à la lecture du fichier, les procédures d'installation du système d'exploitation se lanceront. Dans notre cas, cela nous permet d'installer Debian sur notre machine virtuelle. [^4] 

### Qu’est-ce que MATE ? GNOME ?

**MATE** (prononcé *maté*) est un environnement de bureau délivré en 2011 compatible GTK+3. GTK+3 (GTK pour ***G****imp* ***T****ool**K**it* et +3 pour la version) est un ensemble de bibliothèques logicielles, un ensemble de fonctions permettant de réaliser des interfaces graphiques. Originellement développée pour répondre au besoins du logiciel de traitement d'image GIMP. [^5] [^6] 

**GNOME** pour ***G****NU* ***N****etwork* ***O****bject* ***M****odel* ***E****nvironment* est un environnement de bureau libre dont le but est de rendre accessible l'utilisation de GNU [^7] , un système d'exploitation tenant du noyau d'UNIX. Un noyau, aussi appelé *kernel* gère les ressources de l’ordinateur et permet aux différents composants — matériels et logiciels — de communiquer entre eux. [^8] [^9] 

La différence entre GNOME et MATE est que MATE et un *fork*, une copie modifiée de GNOME, ce qui est rendu possible par le fait que GNOME est open source. [^10] 

### Qu’est-ce qu’un serveur web ?

D'un point de vue matériel, un serveur web est un ordinateur connecté à internet stockant les fichiers composant un site web. [^11]  
Cet ordinateur utilise des logiciels contrôlant l'accèes des utilisateurs au contenu du serveur en suivant le **protocole** HHTP (***H****yper**t**ext* ***T****ransfer* ***P****rotocol*) pour charger et livrer la page demandée par l'utilisateur via son navigateur.  
Un serveur web suit aussi les protocoles SMTP (***S****imple* ***M****ail* ***T****ransfer* ***P****rotocol*) pour traiter les courriers électroniques et FTP (***F****ile* ***T****ransfer* ***P****rotocol*) pour le stockage [^12] 

![shéma serveur web](./images/server-web.png)

### Qu’est-ce qu’un serveur ssh ?

SSH pour ***S****ecure* ***Sh****ell*. Le Shell communément appelé un "terminal", nous permet de gérer des serveurs localement mais aussi à distance grâce au protocole SSH. Ce protocole permet de crypter les échanges entre l'administrateur du serveur et le serveur. Pour comprendre l'utilité du protocole SSH nous pouvons nous pencher sur le protocole qui l'a précédé, le protocole Telnet. Le problème de ce protocole était que les échange entre l'utilisateur et le serveur transitait en texte clair. Ainsi, une personne mal intentionnée pouvait intercepter ces données et les lire, d'où la nécessité d'un protocole chiffré comme le SSH qui a vu le jour en 1995 aujourd'hui normalisé par l'IETF (***I****nternet* ***E****ngineering* ***T****ask* ***F****orce*) dont le but est de faire maintenir des normes sur Internet. [^13] 

### Qu’est-ce qu’un serveur mandataire ?

La plupart du temps un serveur mandataire (aussi appelé *proxy*) est utilisé pour le web, il s'agit alors d'un proxy HTTP. Cependant il peut exister des serveurs proxy pour chaque protocole applicatif (FTP, etc.)  
Dans notre cas, le proxy web fait l'intermédiaire entre le réseau local de l'université et internet. Son rôle principal est de filtrer les sites web que les utilisateurs consultent selon les règles édités (listes blanche/noire). Il est donc **mandaté** par les utilisateurs, d'où son nom.

![shéma proxy](./images/proxy.PNG)

Mais un serveur mandataire peut avoir d'autres fonctions :

* Une fonction de **cache** : garde en mémoire des pages web pour les charger plus rapidement.
* Une fonction de **journal d'activité** (*log*)
* Une fonction d'**authentification** : demandant à l'utilisateur son identifiant et mot de passe pour une couche de sécurité supplémentaire.
* Une fonction de ***load* *balancing***, c'est-à-dire de redistribuer la charge de la requête vers différents serveurs.[^14] [^15] 

## Questions 3 : sudo

### Comment peut-on savoir à quels groupes appartient l’utilisateur user ?

Il y a plusieurs possibilités :\\

1. Avec la commande :  
   `groups userName`
2. Avec la commande :  
   `getent group userName`

   Donnant un résultat du type

   ```
   userName:x:1000:
   root:x:0:
   
   
   ```

   Ce résultat représente quatre champs séparés par des *:* se lisant [^16] [^17] 

   | Attribut | Description |
   |----------|-------------|
   | nom | nom du groupe sur le système |
   | Mot de passe | Selon le symbole, peut indiquer la localisation du mot de passe du groupe pour l'utilisateur en question (en général dans le fichier `shadow`) |
   | ID de groupe | Une chaine décimale unique. La valeur maxiamle est 4 294 967 295 soit 4 Go |
   | Autres utilisateurs du groupe | De la forme user1,user2,etc. Les utilisateurs sont séparés par une virgule |
3. En directement consultant le fichier `/etc/group` avec la commande `nano /etc/group` qui est le fichier donnant le résultat de la commande précédente. [^18] 
4. En tapant la commande :

   ```
   id user
   
   ```

   permettant d'afficher l'UID dans lequel l'utilisateur appartient.

## Questions 4 : Suppléments invités

### Quel est la version du noyau Linux utilisé par votre VM ?

La version du noyau par la VM est : **5\.10.0-21-amd64 x86_64** . Pour le savoir il faut taper la commande `uname -mr`

### À quoi servent les suppléments invités ? Donner 2 principales raisons de les installer.

Lorsque l'on allume la machine virtuelle, nous faisons souvent face à des difficultés pour passer de l'interface hôte et invité (la VM). Par exemple, une fois que la souris est capturée par la VM il faudra systématiquement appuyer sur un raccourci (right ctrl par défaut) pour pouvoir utiliser de nouveau la souris sur l'hôte.

Afin éviter ces problèmes, il est possible d'installer les suppléments invités, des pilotes de périphériques. Ces suppléments peuvent aussi permettre le partage des dossiers, du presse-papier, d'agrandir la fenêtre de la VM conformément à la résolution graphique de l'hôte. [^19] 

### À quoi sert la commande mount (dans notre cas de figure et dans le cas général) ?

La commande **mount** sert à intégrer un périphérique de stockage à l'arborescence de fichiers d'un système. Par exemple lorsque l'on installe un disque dur sur un ordinateur il ne sera pas disponible dans l'arborescence de fichier, il faudra donc faire la commande mount pour le rendre accessible (c'est comme greffer une branche à un arbre). Dans notre cas, la commande va nous permettre d'accéder aux documents de la machine virtuelle par le host. [^20] [^21] 

---

# Rapport technique d'installation de la VM

## Caractéristique de la VM

* Le nom de la VM se nomme **SAe203**
* Il se trouve dans le dossier /usr/local/virtual_machine/infoetu/login (en l'occurence elise.leroy2.etu) \*Il se trouve dans cet emplacement à cause du quota d'espace de stockage
* Le système d'exploitation est Linux, sous la distribution Debian en 64 bits

![](./images/img1.png)

* La machine s'est vue attribuer 2048Mo de mémoire vive et la capacité de son disque dur est de 20Go.

![](./images/img2.png)

## Le fichier ISO monté

* L'ISO utilisé pour démarrer l'installation est debian-11.6.0-amd64-netinst.iso

![](./images/img3.png)

## Installation de l'OS

* Nous avons choisi l'installation par l'interface graphique

![](./images/img4.png)

* La langue et le clavier sont en français et le pays la France

![](./images/img6.png) ![](./images/img7.png) ![](./images/img8.png)

* Le nom de la machine est "serveur"

---

# Sources

Question 1.1)  
[^1]: [Vidéo explicative](https://www.youtube.com/watch?v=_QM7WA4GQzk) d'un ingénieur en génie éléctrique à la retraite, Dave Crabbe, "What does '32-bit' mean ?"  
[^2]: [Site internet](https://www.portables.org/blog/differences-entre-les-versions-32-ou-64-bits-expliques-par-notre-expert-n49) portables.org, site de vente d'ordinateurs, nom de l'auteur inconnu, "Différences entre les versions 32 ou 64 bits expliqués par notre expert"

Question 2.1)  
[^3]: [Site internet](https://www.wooxo.fr/Conseils-Cybersecurite/lexique/Archive-informatique#:~:text=Une%20archive%20est%20un%20fichier,ceux%2Dci%20sont%20souvent%20compress%C3%A9s.) pour la définition simplifiée d'une archive informatique par l'entreprise Wooxo, spécialisé dans la cybersécurité.  
[^4]: [Vidéo explicative](https://www.youtube.com/watch?v=uNhO-mWUD3g) par Ask Leo!, un ingénieur informatique, sur les fichiers ISO.

Question 2.2)  
[^5]: [Site internet](https://ubuntu-mate.org/fr/about/) officiel de Ubuntu MATE  
[^6]: [Site internet communautaire](https://fr.wikipedia.org/wiki/MATE) Wikipédia, "MATE"  
[^7]: [Site internet](https://fr.wikipedia.org/wiki/GTK_\(bo%C3%AEte_%C3%A0_outils\)) Wikipédia, "GTK (boîte à outils)" []: [Site internet](https://doc.ubuntu-fr.org/gnome) Wiki ubuntu-fr, "GNOME"  
[^8]: [Site internet](https://www.malekal.com/quest-ce-que-le-noyau-linux-kernel-role-versions-et-comment-ca-marche/) malekal.com "Qu’est-ce que le Noyau Linux (kernel) : rôle, versions et comment ça marche"  
[^9]: [Site internet](https://www.redhat.com/fr/topics/linux/what-is-the-linux-kernel) officiel de RedHat "Le noyau (ou kernel) Linux, qu'est-ce que c'est ?"  
[^10]: [Site internet communautaire](https://fr.wikipedia.org/wiki/Fork_\(d%C3%A9veloppement_logiciel\)) Wikipédia "Fork (développement logiciel)"

Question 2.3)  
[^11]: [Site internet](https://developer.mozilla.org/fr/docs/Learn/Common_questions/Web_mechanics/What_is_a_web_server) mdn par Mozilla, "Qu'est-ce qu'un serveur web ?"  
[^12]: [Site internet](https://www.hostinger.fr/tutoriels/serveur-web) par l'entreprise d'hébergement web Hostinger, "Qu'est-ce qu'un serveur web?"

Question 2.4)  
[^13]: [Site internet](https://www.it-connect.fr/chapitres/quest-ce-que-ssh/) it-connect, article écrit par Mickael Dorigny, pentester, "Qu'est-ce que SSH ?"

Question 2.5)  
[^14]: [Site internet communautaire](https://web.maths.unsw.edu.au/~lafaye/CCM/lan/proxy.htm) CommentCaMarche.net, "Les serveurs mandataires et relais inverses"  
[^15]: [Site internet communautaire](https://fr.wikipedia.org/wiki/Proxy) Wikipédia, "Proxy"

Question 3.1)  
[^16]: [Site internet](https://linux-note.com/fichier-etcgroup/#:~:text=Structure%20de%20fichier%20%3A%20%2Fetc%2F,par%20le%20symbole%20%C2%AB%20%3A%20%C2%BB.&text=admin%20%3A%20nom%20du%20groupe.,dans%20le%20domaine%20de%20groupe.) Linux - notebook, sur la structure du fichier /etc/group  
[^17]: [Site internet](https://www.ibm.com/docs/fr/aix/7.3?topic=files-etcgroup-file) par IBM sur la structure du fichier /etc/group  
[^18]: les cours du premier semestre d'Introduction aux systèmes d'exploitation.

Question 4.2)  
[^19]: [Site internet](https://doc.ubuntu-fr.org/virtualbox_additions_invite) de la documentation officielle de Ubuntu sur les additions invitées

Question 4.3)  
[^20]: [Forum de discussion](https://unix.stackexchange.com/questions/3192/what-is-meant-by-mounting-a-device-in-linux) du site StackExchange suite à la question "What is meant by mounting a device in linux"

[^21]: [Site internet](http://www.linuxcertif.com/doc/keyword/mount/#:~:text=La%20commande%20mount%20permet%20de,interagissant%20avec%20le%20fichier%20fstab.) linucertif.com, article écrit par Benjamin Poulain, ingénieur informatique, "mount"

Anass AZDOUFAL, Valentin THUILLIER, Elise LEROY